Instructions on how to run my project:

Right now I can't really just up and run the project, this is just a portion of the project. It is a design page for a windows form app. Basically, it only executes in visual studios as it was an incomplete
project all together. Normally what happens when you run the project is that you can design a level for the game Sokoban, adding a hero, destination points, walls, and boxes. To work the design page, you first generate
a grid by inserting the number of rows and columns determining the space of the level, then the user can then proceed to add the objects into the level.

The issue list is in the repository, the project wiki is in the projectwiki.txt file, and the lisence is in
the liscence.txt file, I made my own liscense/copyrights.

https://Chackaphope@bitbucket.org/Chackaphope/assignment1.git is the link to the repository